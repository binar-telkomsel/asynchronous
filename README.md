# ASYNCHRONOUS


### Task
- Buatlah sebuah fungsi asynchronous
- Buatlah sebuah fungsi helper tersendiri untuk fetch API dan di export.
- Gunakan API https://pokeapi.co/api/v2/ sebagai ```BASE_URL```


### Expected Output
```
{
  count: 1118,
  next: "https://pokeapi.co/api/v2/pokemon?offset=100&limit=100",
  previous: null,
  result: [
    {
      name: 'bulbasaur',
      abilities: [
        {
          ability: {
          name: "overgrow",
          url: "https://pokeapi.co/api/v2/ability/65/"
        },
          is_hidden: false,
          slot: 1
        },
        {
          ability: {
          name: "chlorophyll",
          url: "https://pokeapi.co/api/v2/ability/34/"
        },
          is_hidden: true,
          slot: 3
        }
      ],
      base_experience: 64,
      height: 7,
      id: 1,
      is_default: true,
      location_area_encounters: "https://pokeapi.co/api/v2/pokemon/1/encounters",
      types: [
        {
          slot: 1,
          type: {
            name: "grass",
            url: "https://pokeapi.co/api/v2/type/12/"
          }
        },
        {
          slot: 2,
          type: {
            name: "poison",
            url: "https://pokeapi.co/api/v2/type/4/"
          }
        }
      ],
      weight: 69
    },
    ....
  ]
}
```